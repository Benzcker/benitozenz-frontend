# benitozenz
## Inhalte
- X So wichtig ist eine gute Website
- X Ich biete Webentwicklung an
  - X Design
  - X Programmierung
  - X Serverhosting (optional)
  - X Domain (optional) + Verschlüsselung (optional)
- X So kannst du eine Anfrage stellen
- X So viel koste ich (40€/h)
- X So lange dauert die Entwicklung deiner Seite
- X So lange kümmere ich mich um die Seite (2 Jahre nach Veröffentlichung, dann immer 1 Jahr verlängerbar bei beiderseitigem Einverständnis)
- X Impressum
## Seiten
### Main
- So wichtig ist eine gute Website
- Ich biete Webentwicklung an
### Leistungen
- Ich biete Webentwicklung an
  - Design
  - Programmierung
  - Serverhosting (optional)
  - Domain (optional) + Verschlüsselung (optional)
- So kannst du eine Anfrage stellen
- FAQ
  - So viel koste ich (40€/h)
  - So lange dauert die Entwicklung deiner Seite
  - So lange kümmere ich mich um die Seite (2 Jahre nach Veröffentlichung, dann immer 1 Jahr verlängerbar bei beiderseitigem Einverständnis)
### Impressum
### Datenschutz

